package br.com.easytax.customer.controller;

import br.com.easytax.customer.domain.ServiceTax;
import br.com.easytax.iamlib.controller.CrudApi;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/service-tax")
public class ApiServiceTax extends CrudApi<ServiceTax> {


    @PreAuthorize("hasPermission(#id, #this.this.name, 'load') or hasPermission(#id, #this.this.name, 'manage')")
    @PostMapping("/load")
    public ResponseEntity<String> load(@Valid @RequestBody List<ServiceTax> serviceTax) {
        serviceTax.forEach(n -> service.update(n));
        return ResponseEntity.ok("ok");
    }

}
