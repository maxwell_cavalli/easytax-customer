package br.com.easytax.customer.controller;

import br.com.easytax.customer.domain.Customer;
import br.com.easytax.iamlib.controller.CrudApi;
import br.com.easytax.iamlib.security.SecuredContext;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/customer")
@SecuredContext(context = "Customer")
public class ApiCustomer extends CrudApi<Customer> {


}
