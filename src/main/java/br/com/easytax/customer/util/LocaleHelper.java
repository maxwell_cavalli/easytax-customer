package br.com.easytax.customer.util;

import java.util.Arrays;
import java.util.Locale;

public class LocaleHelper {

    private LocaleHelper() {
    }

    public static boolean validateIsoCode(final String code) {
        return Arrays.stream(Locale.getISOCountries())
                .anyMatch(s -> s.equals(code));
    }


}
