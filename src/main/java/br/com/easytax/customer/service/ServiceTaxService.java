package br.com.easytax.customer.service;

import br.com.easytax.customer.domain.ServiceTax;
import br.com.easytax.iamlib.service.BaseService;
import org.springframework.stereotype.Service;

@Service
public class ServiceTaxService extends BaseService<ServiceTax> {

}
