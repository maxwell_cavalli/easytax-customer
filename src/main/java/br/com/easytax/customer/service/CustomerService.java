package br.com.easytax.customer.service;

import br.com.easytax.customer.domain.Customer;
import br.com.easytax.customer.service.address.AddressServiceAPI;
import br.com.easytax.customer.util.CpfCnpjCheck;
import br.com.easytax.customer.util.LocaleHelper;
import br.com.easytax.iamlib.exception.BadRequestException;
import br.com.easytax.iamlib.service.BaseService;
import org.springframework.stereotype.Service;

@Service
public class CustomerService extends BaseService<Customer> {

    private final ServiceTaxService serviceTax;
    private final AddressServiceAPI addressServiceAPI;

    public CustomerService(final ServiceTaxService serviceTax,
                           final AddressServiceAPI addressServiceAPI) {
        this.serviceTax = serviceTax;
        this.addressServiceAPI = addressServiceAPI;
    }

    @Override
    public Customer beforeUpdate(final Customer previous, final Customer current) {
        if (!CpfCnpjCheck.isValid(current.getCnpj())) {
            throw new BadRequestException("Invalid CNPJ");
        }

        if (!LocaleHelper.validateIsoCode(current.getAddress().getCountry())) {
            throw new BadRequestException("Invalid country code");
        }

        current.getServiceTax()
                .forEach(s -> serviceTax.get(s)
                        .orElseThrow(() -> new BadRequestException(String.format("Invalid TaxCode %s", s)))
                );

        current.getCityCode()
                .forEach(s -> addressServiceAPI.retrieveCityById(s)
                        .orElseThrow(() -> new BadRequestException(String.format("Invalid City %s", s)))
                );

        return super.beforeUpdate(previous, current);
    }

    @Override
    public Customer beforeCreate(final Customer customer) {
        if (!CpfCnpjCheck.isValid(customer.getCnpj())) {
            throw new BadRequestException("Invalid CNPJ");
        }

        if (!LocaleHelper.validateIsoCode(customer.getAddress().getCountry())) {
            throw new BadRequestException("Invalid country code");
        }

        return super.beforeCreate(customer);
    }
}
