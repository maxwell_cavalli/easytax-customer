package br.com.easytax.customer.domain;

import br.com.easytax.iamlib.domain.RealmIsolatedDomain;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@Document("customers")
@CompoundIndex(def = "{'id': 1}")
public class Customer extends RealmIsolatedDomain {

    @NotBlank(message = "is required")
    private String companyName;

    @NotBlank(message = "is required")
    private String tradeName;

    @NotBlank(message = "is required")
    private String cnpj;

    @NotBlank(message = "is required")
    private String contactName;

    @NotBlank(message = "is required")
    private String phone;

    @NotBlank(message = "is required")
    private String email;

    @Valid
    @NotNull(message = "is required")
    private Address address;

    @Valid
    @NotNull(message = "is required")
    private Set<String> serviceTax;

    @Valid
    @NotNull(message = "is required")
    private Set<String> cityCode;

    private String externalIdentifier;

}
