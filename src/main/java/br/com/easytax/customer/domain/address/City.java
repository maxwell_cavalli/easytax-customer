package br.com.easytax.customer.domain.address;

import lombok.Data;

@Data
public class City {

    private String id;
    private Integer code;
    private String name;
    private String country;


}
