package br.com.easytax.customer.domain;

import br.com.easytax.iamlib.domain.BaseDomain;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;

@Data
@Document("service_tax")
public class ServiceTax extends BaseDomain {

    @NotBlank(message = "is required")
    private String code;

    @NotBlank(message = "is required")
    private String name;

}
