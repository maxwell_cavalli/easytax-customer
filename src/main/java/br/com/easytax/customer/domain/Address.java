package br.com.easytax.customer.domain;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class Address  {

    @NotBlank(message = "is required")
    private String street;

    @NotBlank(message = "is required")
    private String number;

    @NotBlank(message = "is required")
    private String region;

    @Pattern(regexp = "^[A-Z]{2}$", message = "Invalid country code. Use count ISO codes ")
    @NotBlank(message = "is required")
    private String country;

}
