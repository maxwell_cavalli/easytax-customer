package br.com.easytax.customer.repository;

import br.com.easytax.customer.domain.Customer;
import br.com.easytax.iamlib.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends BaseRepository<Customer> {

}
