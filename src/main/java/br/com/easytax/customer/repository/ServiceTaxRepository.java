package br.com.easytax.customer.repository;

import br.com.easytax.customer.domain.ServiceTax;
import br.com.easytax.iamlib.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceTaxRepository extends BaseRepository<ServiceTax> {

}
